﻿% Autor: Jeffrey Remien
% Datum: 27.10.2016

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TEIL 1  s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

is_a_list([]). %Leere Liste ist Liste
is_a_list([_H|T]):- is_a_list(T). %Listen Elemente nacheinander entfernen bis leere Liste erreicht ist. Sofern es keinen Fail gibt handelt es sich um eine Liste

app([],L2,L2). %sofern die erste Liste Leer ist, ist das ergebnis die zweite. Bzw. ist der Punkt in der rekursion erreicht, dass man am tiefsten Punkt der ersten Liste ist kann nun die zweite als Tail übergeben werden. Beim rückwärts heraussprigen aus den Iterationsschritten wird nun jeweils das letzte Listen Element aus L1 angehängt
app([H|L1],L2,[H|L3]):- app(L1,L2,L3).

praefix([F|[]],[F|T]):- T \== []. %/== prüft auf ungleich /= hätte auf nicht unifizierbar geprüft und wäre im falle einer variable falsch
praefix([P|T1],[P|T2]):- praefix(T1,T2).

%Helper um Elementweise zu vergleichen ob Das Präfix ohne Unterbrechun vorkommt
%praefixHlp([P|[]],[P|T]):- T \= []. % Ende des Verglleichs, testen ob Die Vergleichsliste nicht zu Ende ist sonst Fehlschlag.
%praefixHlp([F|T1],[F,T2]):- praefixHlp(T1,T2). %Beide Listen für fortlaufenden Vergleich weiterschalten
%praefixHlp(L,[_P|T2]):- praefixHlp(L,T2). %zu testende Liste weiterschalten
%Präfix Funktion
%praefix(P, L):- praefixHlp(P,L). %Aktuelle Liste auf Präfix testen.
%praefix(P, [_H,T]):- praefix(P,T). %Falls nicht (vollständig) gefunden x

suffix([], _L):- !, fail.
suffix(S, [_H|S]).
suffix(S, [_H|T]):- suffix(S,T).

%suffixHlp([S|[]],[_X,S|_T]).
%suffixHlp(
%suffix([], _L):- !, fail. %leere Liste darf kein Suffix sein.
%suffix(S, [_H|S]). %Ist S Head der Subliste im Tail der aktuellen liste kommt mindestens ein Element vor ihm
%suffix(S, [_H|T]):- suffix(S,T). %Liste Reduzieren bis S eventuell zu finden ist. Ansonsten Fehlschlag

%infixHlp(_I,[]):- !,fail.
infixHlp([I|T1],[_X,I|T2]):- infixHlp(T1,[I|T2]).
infixHlp([I|[]],[_X,I|T]):- T \== [].
infix([],_L):- fail.
infix(I,L):- infixHlp(I,L).
infix(I,[_HL|TL]):- infix(I,TL).

%infix(I,L):- praefix(I,L),suffix(I,L). %Infix muss sowohl suffix als auch Präfix sein

%Funktion die Feststellt ob eine Liste eine Gerade Anzahl an Elementen hat
even([]). %leere Listen werden als Gerade gewertetc
even([_X,_Y|R]):- even(R). %Elemente Paarweise aus der Liste entfernen.
%Funktion um von einer Liste alle Sublisten zu finden und mit eo_count aufzurufen.
eo_sublists([],0,0). %Keine Elemente oder Subliste enthalten, Abbruch.
%eo_sublists([],_Even,_Odd). %Ergebnisrückgabe
eo_sublists([H|T], Even, Odd):- is_a_list(H), !, eo_count(H, Even1,Odd1), eo_sublists(T, Even2, Odd2), Even = Even1 + Even2, Odd = Odd1 + Odd2. %Head von aktueller Liste ist selbst liste also wird eo_count aufgerufen um diese zu zählen und dessen Ergebnis mit dem Ergebnis der Restlichen Elemente der aktuellen Liste aufaddiert
eo_sublists([_H|T], Even, Odd):- eo_sublists(T, Even1, Odd1), Even = Even1, Odd = Odd1. %Head ist selbst keine Liste also nur für die folgenden Elemente prüfen und aufaddieren.
%Funktion um eine Liste je nachdem ob gerade oder Ungerade auf den jeweiligen Zähler zu addieren und eo_sublists mit ihr aufzurufen.
eo_count(L,Even,Odd):- even(L), !, eo_sublists(L, Even1,Odd1), Even is Even1 + 1, Odd is Odd1. %Liste ist gerade
eo_count(L,Even,Odd):- eo_sublists(L, Even1, Odd1), Even is Even1, Odd is Odd1 + 1. %Liste ist ungerade

del_element(_E, [], []). %aus leerer Liste kann nichts entfernt werden. Rückgabe ebenfalls leer
del_element(E, [E|T], R):- !, del_element(E, T, R). %Wurde das Element in der Liste gefunden wird dieses nicht übernommen.
del_element(E, [H1|T1], [H1|T2]):- del_element(E,T1,T2). %Ein Element, das sich vom gesuchten unterscheidet wird übernommen

substitute(_E1, _E2, [], []). %in leerer Liste kann nichts ersetzt werden
substitute(E1, E2, [E1|T1], [E2|T2]):- !, substitute(E1, E2, T1, T2). %Wenn E1 gefunden wurde durch E2 im Ergebnis ersetzen
substitute(E1, E2, [H|T1], [H|T2]):- substitute(E1, E2, T1, T2). %Falls nicht gefunden Listen weiterschalten.
%substitute(E1, E2, L1, L2):- substitute(E1, E2, L1, []).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TEIL 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


nat(0).
nat(s(X)) :-nat(X).


nat2s(0,0). % 0 ist als 0 definiert auf beiden Seiten. Gleichzeitig Abbruchkriterium.
nat2s(Zahl, s(SZahl)):- nat2s(Tmp,SZahl), Zahl is Tmp + 1. % für jedes s() 1 auf die Natürliche Zahl addieren

s2nat(Szahl,Zahl):- nat2s(Zahl,Szahl). %Da Prolog in nat2s je nachdem nach welcher Variable man fragt passend einsetzt muss sich die Umkehrfunktion nur nat2s bedienen.

%inc(0, StartAt).
%inc(s(Amount),StartAt):- inc(Amount, s(StartAt)).

add(0,Summand2,Summand2). %Ist der erste Summand 0 so ist das Ergebnis der zweite. Gleichzeitig Abbruchkriterium für den rekursiven Aufruf.
add(s(Summand1),Summand2,Summe):- add(Summand1,s(Summand2),Summe). %Klammern vom ersten Summand entfernen und rekursiv dem Zweiten "anhängen" 

sub(0,_Subtrahend,0).  %Per Definition soll das Ergebnis immer 0 sein wenn VON 0 abgezogen wird
sub(Minuend,0,Minuend). %Ist der Subtrahend 0 entspricht der Minuend dem Ergebnis. Gleichzeitig Abbruchkriterium für den rekursiven Aufruf.
sub(s(Minuend),s(Subtrahend),Differenz):- sub(Minuend,Subtrahend,Differenz). %Umschließende s() von Minuend und Subtrahend entfernen bis einer von beiden 0 wird. Die oberen beiden Regeln entscheiden dann das Ergebnis.

mul(0,_Faktor2,0). %Fall, dass erster Faktor 0 ist, keine Operation.
mul(Faktor1,s(0),Faktor1). %Fall, dass zweiter Faktor 1 ist, in dem Falle ist das Ergebnis Faktor1
mul(0,_Faktor2,_Produkt). %Abbruchkriterium für das Sequenzielle Aufaddieren
mul(s(Faktor1),Faktor2,Produkt):- mul(Faktor1,Faktor2,Temp), add(Faktor2,Temp,Temp2), Produkt = Temp2. %Faktor1 runterzählen und für jedes Mal Faktor2 aufaddieren und am Ende als Produkt zurück geben. Die Sonderfälle Faktor1 = 0 und Faktor1 = 1 sind automatisch abgedeckt. Im ersten fall wird erst gar nicht aufaddiert, im zweiten Fall wird nur einmal aufaddiert.

power(_B, 0, s(0)). %B hoch 0 ist 1
power(B, s(0), B). %Muster welches Initialwert für die Rekursive Suche liefert
power(_B, s(0), _R). %Ergebnismuster für die Rekursive suche
power(B, s(E), R):- power(B, E, Temp), mul(B, Temp, Temp2), R = Temp2. %E runterzählen und entsprechend immer wieder B aufmultiplizieren mittels mul

fac(0,s(0)). % !0 = 1
fac(s(0),s(0)). %!1 = 1
fac(s(0),_Factorial). %Abbruchkriterium für Rekursive aufrufe
fac(s(N), Factorial):- fac(N, TempFac), mul(s(N),TempFac,TempMul), Factorial = TempMul. %Rekursives aufmultiplizieren, n jedes mal veringern und auf das zwischenergebnis multiplizieren bis 1 erreicht wurde

lt(N1,N2):-  sub(N2,N1,Temp), Temp \= 0. %wird nur wahr wenn N2 tatsächlich größer ist, da bei gleichen werten bzw. dem Abziehen einer größeren Zahl durch die Implementierung von sub immer 0 herauskommt.

mods(_A, 0, _C):- !,fail. %durch null nicht teilbar, ergebnis schlägt fehl
mods(A, B, C):- lt(A,B), C = A. %Abbruchkriterium für rekursive suche nach dem Rest. ist A kleiner als B entspricht es dem Rest (entweder bereits zu beginn oder nach mehrmaligem Abziehen von B)
mods(A, B, C):- sub(A, B, Temp), mods(Temp,B,Temp2), C = Temp2. %B so lange von A abziehen bis dieses kleiner B ist


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

test(is_a_list) :-
    %positiv
    write('is_a_list +'),
    is_a_list([]), write('.'),!,
    is_a_list([1]), write('.'),!,
    is_a_list([1, 2]), write('.'),!,
    is_a_list([1, 2,[a,[b,[c],d]]]),
    %negativ
    write('. -'),!,
    \+ is_a_list([kopf|rest]), write('.'),!,
    \+ is_a_list((kopf,rest)), write('.'),!,
    \+ is_a_list((_Kopf,_Rest)), write('.'),!,
    \+ is_a_list(liste),!, writeln('ok').

test(app) :-
    write('app'),
    app([1], [2], [1, 2]), write('.'),!,
    app([], [1], [1]), write('.'),!,
    app([1], [], [1]), write('.'),!,
    app([1,2,3], [a,b,c], [1, 2, 3, a, b, c]), write('.'),!,
    app([[1]], [[a,b],c,[d]], [[1],[a,b],c,[d]]),!, writeln('ok').

test(infix) :-
    %positiv
    write('infix +'),
    infix([2,3], [1, 2, 3, 4]), write('.'),!,
    infix([2,3], [1, 2, 9, 2, 3, 4]), write('.'),!,
    infix([2,3], [1, 3, 9, 2, 3, 4]),
    %negativ
    write('. -'),!,
    \+ infix([], [1, 2, 3, 4]), write('.'),!,
    \+ infix([1,2], [1, 2, 3, 4]), write('.'),!,
    \+ infix([3,4], [1, 2, 3, 4]), write('.'),!,
    \+ infix([4], [1, 2, 3, 4]), write('.'),!,
    \+ infix([2,3], [1, 2, 5, 6, 3, 4]), write('.'),!,
    \+ infix([2, 3], [1, [2, 3],4]),!, writeln('ok').

test(suffix) :-
    %positiv
    write('suffix +'),
    suffix([3], [1, 2, 3]), write('.'),!,
    suffix([2, 3], [1, 2, 3]), write('.'),
    %negativ
    write('. -'),!,
    \+ suffix([], [1, 2, 3]), write('.'),!,
    \+ suffix([1, 2, 3], [1, 2, 3]), write('.'),!,
    \+ suffix([2, 3], [1, 2, 4, 3]), write('.'),!,
    \+ suffix([2, 3], [1, [2, 3]]),!, writeln('ok').

test(praefix) :-
    %positiv
    write('praefix +'),
    praefix([1], [1, 2, 3]), write('.'),!,
    praefix([1, 2], [1, 2, 3]), write('.'),
    %negativ
    write('. -'),!,
    \+ praefix([], [1, 2, 3]), write('.'),!,
    \+ praefix([1, 2, 3], [1, 2, 3]), write('.'),
    \+ praefix([1, 2], [1, 4, 2, 3]), write('.'),!,
    \+ praefix([1, 2], [[1, 2], 3]),!, writeln('ok').

test(eo_count) :-
    write('eo_count'),
    eo_count([],1,0), write('.'),!,
    eo_count([1,2],1,0), write('.'),!,
    eo_count([1],0,1), write('.'),!,
    eo_count([1,[],[1],4,[1,2],[1,2,3],7],2,3), write('.'),!,
    eo_count([1,[1,[],[1],4,[1,2],[1,2,3],7],[1],4,[1,2],[1,2,3],7],3,6),!, writeln('ok').

test(del_element) :-
    write('del_element'),
    del_element(1, [1, 2, 3], [2, 3]), write('.'),!,
    del_element(2, [1, 2, 3], [1, 3]), write('.'),!,
    del_element(3, [1, 2, 3], [1, 2]), write('.'),!,
    del_element([a,b], [1, [a,b], 3], [1, 3]),!, writeln('ok').

test(substitute) :-
    write('substitute'),
    substitute(1,a,[0,1,2,3,1,5],[0,a,2,3,a,5]), write('.'),!,
    substitute(1,a,[0,2,3,0,5],[0,2,3,0,5]), write('.'),!,
    substitute(1,a,[1,1,1,1,1],[a,a,a,a,a]), write('.'),!,
    substitute(1,a,[1],[a]), write('.'),!,
    substitute(1,a,[],[]),!, writeln('ok').

test(nat) :-
    %positiv
    write('nat +'),
    nat(0), write('.'),!,
    nat(s(0)), write('.'),!,
    nat(s(s(0))),
    %negativ
    write('. -'),!,
    \+ nat(42), write('.'),!,
    \+ nat(null), write('.'),!,
    \+ nat(s(s(1))),!, writeln('ok').

test(nat2s) :-
    write('nat2s'),
    nat2s(0, 0), write('.'),!,
    nat2s(1, s(0)), write('.'),!,
    nat2s(2, s(s(0))), write('.'),!,
    nat2s(3, s(s(s(0)))),!, writeln('ok').

test(add) :-
    write('add'),
    add(0, 0, 0), write('.'),!,
    add(s(0), 0, s(0)), write('.'),!,
    add(0,s(0), s(0)), write('.'),!,
    add(s(0), s(0), s(s(0))), write('.'),!,
    add(s(0), s(s(0)), s(s(s(0)))), write('.'),!,
    add(s(s(s(0))), s(s(s(s(0)))), s(s(s(s(s(s(s(0)))))))),!, writeln('ok').

test(sub) :-
    write('sub'),
    sub(0, 0, 0), write('.'),!,
    sub(s(s(0)), s(0), s(0)), write('.'),!,
    sub(s(s(s(s(s(s(s(0))))))), s(s(s(s(s(s(s(0))))))), 0), write('.'),!,
    sub(s(0), s(s(s(s(s(s(s(0))))))), 0), write('.'),!,
    sub(s(s(s(s(s(s(s(0))))))), s(s(s(s(0)))),s(s(s(0)))),!, writeln('ok').

test(mul) :-
    write('mul'),
    mul(0,s(s(s(0))), 0), write('.'),!,
    mul(s(s(0)), 0, 0), write('.'),!,
    mul(s(s(0)), s(s(s(0))), s(s(s(s(s(s(0))))))), write('.'),!,
    mul(s(s(0)), s(0), s(s(0))), write('.'),!,
    mul(s(0), s(0), s(0)),!, writeln('ok').

test(power) :-
    write('power'),
    power(0, 0, s(0)), write('.'),!,
    power(s(s(s(s(0)))), 0, s(0)), write('.'),!,
    power(0, s(0), 0), write('.'),!,
    power(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0)))))))))))))))), s(0), s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))))))), write('.'),!,
    power(s(0), s(s(0)), s(0)), write('.'),!,
    power(s(s(0)), s(s(0)), s(s(s(s(0))))), write('.'),!,
    power(s(s(0)), s(s(s(0))), s(s(s(s(s(s(s(s(0))))))))), write('.'),!,
    power(s(s(s(s(0)))), s(s(0)), s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))))))),!, writeln('ok').

test(fac) :-
    write('fac'),
    fac(0, s(0)), write('.'),!,
    fac(s(0), s(0)), write('.'),!,
    fac(s(s(0)), s(s(0))), write('.'),!,
    fac(s(s(s(s(0)))), s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))))))))))))))),!, writeln('ok').

test(lt) :-
    %positiv
    write('lt +'),
    lt(0, s(0)), write('.'),!,
    lt(s(s(0)), s(s(s(s(s(s(s(s(0))))))))), write('.'),!,
    %negativ
    write('. -'),!,
    \+ lt(0, 0), write('.'),!,
    \+ lt(s(s(s(s(s(s(s(s(0)))))))), s(s(s(s(s(s(s(s(0))))))))), write('.'),!,
    \+ lt(s(s(s(s(s(s(s(s(0)))))))), s(s(0))),!, writeln('ok').

test(mods) :-
    write('mods'),
    mods(s(s(s(s(s(s(s(s(0)))))))), s(0), 0), write('.'),!,
    mods(s(s(s(s(s(0))))), s(s(s(s(s(0))))), 0), write('.'),!,
    mods(s(s(s(s(s(s(s(s(0)))))))), s(s(s(s(s(0))))), s(s(s(0)))), write('.'),!,
    mods(s(s(s(0))), s(s(s(s(s(0))))), s(s(s(0)))), write('.'),!,
    %negativ
    write('. -'),!,
    \+ mods(s(s(s(0))), 0, _Something),!, writeln('ok').

test(s2nat) :-
    write('s2nat'),
    s2nat(0, 0), write('.'),!,
    s2nat(s(0),1), write('.'),!,
    s2nat(s(s(0)),2), write('.'),!,
    s2nat(s(s(s(s(s(s(s(s(0)))))))),8),!, writeln('ok').
        
test(alllist) :-
    test(is_a_list),
    test(app),
    test(infix),
    test(suffix),
    test(praefix),
    test(eo_count),
    test(del_element),
    test(substitute)
    .
test(allnat) :-
    test(nat),
    test(nat2s),
    test(add),
    test(sub),
    test(mul),
    test(power),
    test(fac),
    test(lt),
    test(mods),
    test(s2nat)
    .

test(memberCount) :-
    write('memberCount'),
    memberCount(a,[b,c,d,e],0), write('.'),!,
    memberCount(a,[a,b,c,d,e],1), write('.'),!,
    memberCount(a,[f,b,c,d,a],1), write('.'),!,
    memberCount(a,[f,b,c,d,a,d,c,b,f],1), write('.'),!,
    memberCount(a,[a,a,a,a,a],5), writeln('ok').
